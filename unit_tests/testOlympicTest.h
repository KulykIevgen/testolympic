#ifndef TESTOLYMPIC_TESTOLYMPICTEST_H
#define TESTOLYMPIC_TESTOLYMPICTEST_H

#include "gtest/gtest.h"

class testOlympicTest
{
public:
    static void TestGtestLibLink();
};

TEST(testOlympicTest, TestGtestLibLink)
{
    testOlympicTest::TestGtestLibLink();
}

class testAdder
{
public:
    static void TestAddTwoPositives();
    static void TestAddTwoNegatives();
    static void TestAddNegativeToPositive();
    static void TestAddPositiveToNegative();
    static void TestAddBigValuesForOverflow();
};

TEST(testAdder, TestAddTwoPositives)
{
    testAdder::TestAddTwoPositives();
}
TEST(testAdder, TestAddTwoNegatives)
{
    testAdder::TestAddTwoNegatives();
}
TEST(testAdder, TestAddNegativeToPositive)
{
    testAdder::TestAddNegativeToPositive();
}
TEST(testAdder, TestAddPositiveToNegative)
{
    testAdder::TestAddPositiveToNegative();
}
TEST(testAdder, TestAddBigValuesForOverflow)
{
    testAdder::TestAddBigValuesForOverflow();
}

class testLargetSumm
{
public:
    static void ExactLargetSumm();
    static void LessButNearLargestSumm();
};

TEST(testLargetSumm, ExactLargetSumm)
{
    testLargetSumm::ExactLargetSumm();
}
TEST(testLargetSumm, LessButNearLargestSumm)
{
    testLargetSumm::LessButNearLargestSumm();
}

#endif //TESTOLYMPIC_TESTOLYMPICTEST_H
