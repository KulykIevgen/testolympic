#include "testOlympicTest.h"

#include <adder.h>
#include <largest_sum.h>

#include <cstdint>
#include <limits>
#include <stdexcept>
#include <vector>

void testOlympicTest::TestGtestLibLink()
{
    EXPECT_EQ(1,1);
}

void testAdder::TestAddTwoPositives()
{
    auto result = adder::add(20, 35);
    EXPECT_EQ(result, static_cast<int> (55));
}

void testAdder::TestAddTwoNegatives()
{
    auto result = adder::add(-20, -35);
    EXPECT_EQ(result, static_cast<int> (-55));
}

void testAdder::TestAddNegativeToPositive()
{
    auto result = adder::add(20, -35);
    EXPECT_EQ(result, static_cast<int> (-15));
}

void testAdder::TestAddPositiveToNegative()
{
    auto result = adder::add(-20, 35);
    EXPECT_EQ(result, static_cast<int> (15));
}

void testAdder::TestAddBigValuesForOverflow()
{
    auto max_int = std::numeric_limits<int>::max();
    EXPECT_THROW(auto result = adder::add(max_int, max_int), std::exception);
}

void testLargetSumm::LessButNearLargestSumm()
{
    std::vector<uint32_t> values{100, 3, 7};
    std::vector<uint32_t> result;
    uint32_t summ;

    LS::find_largest_sum(12, values, result, summ);
    EXPECT_EQ(summ, static_cast<uint32_t> (10));
    EXPECT_EQ(result.size(), 2U);
}

void testLargetSumm::ExactLargetSumm()
{
    std::vector<uint32_t> values{2, 3, 4, 7};
    std::vector<uint32_t> result;
    uint32_t summ;

    LS::find_largest_sum(11, values, result, summ);
    EXPECT_EQ(summ, static_cast<uint32_t> (11));
    EXPECT_EQ(result.size(), 2U);
}
