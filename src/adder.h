#ifndef TESTOLYMPIC_ADDER_H
#define TESTOLYMPIC_ADDER_H

namespace adder
{
	/*!
	 * addes two integers
	 * @param a - first integer
	 * @param b - second integer
	 * @return a + b or throws exception in case of overflow
	 */
    int add(int a, int b);
}

#endif //TESTOLYMPIC_ADDER_H
