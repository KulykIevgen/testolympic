#include "largest_sum.h"

#include <algorithm>
#include <cstdint>
#include <functional>
#include <vector>
#include <iostream>

void LS::find_largest_sum(const uint32_t T, const std::vector<uint32_t>& I,
                          std::vector<uint32_t>& M, uint32_t& S)
{
    std::vector<std::vector<uint32_t>> target(I.size() + 1, 
		std::vector<uint32_t> (T + 1));
	S = 0;

	for (uint32_t k = 1; k <= I.size(); k++)
	{
		for (uint32_t x = 1; x <= T; x++)
		{
			if (x >= I[k-1])
			{
				target[k][x] = std::max(target[k - 1][x],
					target[k - 1][x - I[k - 1]] + I[k - 1]);
			}
			else
			{
				target[k][x] = target[k - 1][x];
			}
		}
	}

	std::function<void(uint32_t, uint32_t)> findAnswer = [&](uint32_t k, uint32_t s) -> void
	{
		if (!target[k][s])
		{
			return;
		}

		if (target[k - 1][s] == target[k][s])
		{
			return findAnswer(k - 1, s);
		}
		else
		{
			findAnswer(k - 1, s - I[k - 1]);
			M.push_back(I[k - 1]);
			S += I[k - 1];
		}
	};

	findAnswer(I.size(), T);
}
