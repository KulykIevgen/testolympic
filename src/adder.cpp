#include "adder.h"

#include <limits>
#include <stdexcept>

int adder::add(int a, int b)
{
    const auto top = static_cast<unsigned int> (std::numeric_limits<int>::min());
    while (b != 0)
    {
        const auto u_a = static_cast<unsigned int> (a);
        const auto u_b = static_cast<unsigned int> (b);
        const auto carry = static_cast<int> (u_a & u_b);
        a = static_cast<int> (u_a ^ u_b);

        int test = (carry >= 0) ? carry : -carry;
        if ((static_cast<unsigned int> (test) << 1) > top)
        {
            throw std::exception("Overflow detected");
        }

        b = carry << 1;
    }
    return a;
}
