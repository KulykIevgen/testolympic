#ifndef TESTOLYMPIC_LARGEST_SUM_H
#define TESTOLYMPIC_LARGEST_SUM_H

#include <cstdint>
#include <vector>

namespace LS
{
	/*!
	 * solves knapsack problem when 	 
	 * @param T - max knapsack weight
	 * @param I - weights and costs
	 * @param M - vector of items in knapsack
	 * @param S - summ of weights
	 * @return nothing, modifies M and S
	 */
    void find_largest_sum(const uint32_t T, const std::vector<uint32_t>& I,
                          std::vector<uint32_t>& M, uint32_t& S);
}

#endif //TESTOLYMPIC_LARGEST_SUM_H
