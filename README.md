# How to build the project on Windows.

* Install [cmake](https://github.com/Kitware/CMake/releases/download/v3.14.0/cmake-3.14.0-win64-x64.msi), [Python3 for gtest](https://www.python.org/ftp/python/3.7.4/python-3.7.4-amd64.exe), [Visual Studio 2019](https://visualstudio.microsoft.com/ru/thank-you-downloading-visual-studio/?sku=Community&rel=15) and [Doxygen](https://sourceforge.net/projects/doxygen/files/latest/download)
* `mkdir build`
* `cd build`
* `cmake .. -G "Visual Studio 16 2019"`
* `cmake --build . --target ALL_BUILD --config RelWithDebInfo`
* `ctest -VV -C RelWithDebInfo`